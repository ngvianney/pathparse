const fs = require('fs');
const path = require('path');

const input = path.join(__dirname + '/in_file.txt');
let output = path.join(__dirname + '/output.txt');

const deleteOutputFile = (content) => {
   fs.unlink(output, (err) => {
       if(err) { console.log('error deleting output file'); process.exit(1); }
       writeToFile(content);
   })
};

const writeToFile = (content) => {
    fs.writeFile(output, content, (err) => {
        if (err) {
            console.log('error writing to output file');
            process.exit(1);
        }
        console.log('operation successful!');
    });
}

fs.readFile(input, (err, file) => {
    if (err) {
        process.exit(1);
    }
    let fileAsString = file.toString().replace(/\r?\n|\r/g, '');
    let content = fileAsString.match(/C:.*[a-z0-9](.*.txt)/gim);
    if (content.length == 0) process.exit(1);
    pathCollection = content[0].match(/c:.*?(.txt)/gim);
    if (pathCollection.length == 0) process.exit(1);
    fs.exists(output, (exists) => {
        let outputString = pathCollection.toString().replace(/,/g, '\n');
        if (exists === true) { deleteOutputFile(outputString); return; };
        writeToFile(outputString);
    });
});



